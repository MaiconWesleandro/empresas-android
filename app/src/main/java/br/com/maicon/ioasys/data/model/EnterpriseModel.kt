package br.com.maicon.ioasys.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class EnterpriseModel(

    @SerializedName("id")
    val id: Int?,

    @SerializedName("email_enterprise")
    val emailEnterprise: String?,

    @SerializedName("facebook")
    val facebook: String?,

    @SerializedName("twitter")
    val twitter: String?,

    @SerializedName("linkedin")
    val linkedin: String?,

    @SerializedName("phone")
    val phone: String?,

    @SerializedName("own_enterprise")
    val ownEnterprise: String?,

    @SerializedName("enterprise_name")
    val enterpriseName: String?,

    @SerializedName("photo")
    val photo: String?,

    @SerializedName("description")
    val description: String?,

    @SerializedName("city")
    val city: String?,

    @SerializedName("country")
    val country: String?,

    @SerializedName("value")
    val value: Int?,

    @SerializedName("share_price")
    val sharePrice: Double?,

    @SerializedName("enterprise_type")
    val enterpriseType: EnterpriseTypeModel?

) : Parcelable
