package br.com.maicon.ioasys.ui.home.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import br.com.maicon.ioasys.R
import br.com.maicon.ioasys.data.model.EnterpriseModel
import kotlinx.android.synthetic.main.enterprise_card.view.*

class EnterpriseAdapter(val list: List<EnterpriseModel>, val listener: OnItemClickListener<EnterpriseModel>) :
    RecyclerView.Adapter<EnterpriseAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {

        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(
                R.layout.enterprise_card,
                parent,
                false
            )
        )
    }

    override fun getItemCount(): Int = list.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = list[position]
        holder.bindItemView(item) {
            listener.onItemClick(item, position)
        }

    }

    inner class ViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bindItemView(item: EnterpriseModel, onItemClick: () -> Unit) {

            view.apply {
                textViewEnterpriseName.text = item.enterpriseName
                textViewType.text = item.enterpriseType?.enterpriseTypeName
                textViewCountry.text = item.country
                setOnClickListener { onItemClick() }

            }
        }
    }
}