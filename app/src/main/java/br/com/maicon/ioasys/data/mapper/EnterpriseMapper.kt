package br.com.maicon.ioasys.data.mapper

import br.com.maicon.ioasys.data.model.EnterpriseModel
import br.com.maicon.ioasys.data.model.EnterpriseResponse
import br.com.maicon.ioasys.data.model.EnterpriseTypeModel

object EnterpriseMapper {
    fun parse(enterpriseResponse: EnterpriseResponse?): List<EnterpriseModel> {
        val enterpriseList = enterpriseResponse?.enterprises?: listOf()
        return enterpriseList.map { parseEnterprise(it) }
    }

    private fun parseEnterprise(enterpriseModel: EnterpriseModel?): EnterpriseModel{
        return EnterpriseModel(
            id = enterpriseModel?.id ?: 0,
            emailEnterprise = enterpriseModel?.emailEnterprise ?: "",
            facebook = enterpriseModel?.facebook ?: "",
            twitter = enterpriseModel?.twitter ?: "",
            linkedin = enterpriseModel?.linkedin ?: "",
            phone = enterpriseModel?.phone ?: "",
            ownEnterprise = enterpriseModel?.ownEnterprise ?: "",
            enterpriseName = enterpriseModel?.enterpriseName ?: "",
            photo = enterpriseModel?.photo ?: "",
            description = enterpriseModel?.description ?: "",
            city = enterpriseModel?.city ?: "",
            country = enterpriseModel?.country ?: "",
            value = enterpriseModel?.value ?: 0,
            sharePrice = enterpriseModel?.sharePrice ?: 0.0,
            enterpriseType = parseEnterpriseType(enterpriseModel?.enterpriseType)
        )
    }

    private fun parseEnterpriseType(enterpriseTypeModel: EnterpriseTypeModel?):EnterpriseTypeModel{
        return EnterpriseTypeModel(
            id = enterpriseTypeModel?.id ?: 0,
            enterpriseTypeName = enterpriseTypeModel?.enterpriseTypeName ?: ""
        )

    }
}