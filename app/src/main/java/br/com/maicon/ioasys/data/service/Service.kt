package br.com.maicon.ioasys.data.service

import br.com.maicon.ioasys.data.model.EnterpriseResponse
import br.com.maicon.ioasys.data.model.LoginModel
import br.com.maicon.ioasys.data.model.SignInResponse
import retrofit2.Response
import retrofit2.http.*

interface Service {
    @POST(SIGN_IN_PATH)
    suspend fun signIn(@Body loginModel: LoginModel): Response<SignInResponse>

    @GET(GET_ENTERPRISE_LIST_PATH)
    suspend fun getEnterprises(
        @Header("access-token") access_token: String,
        @Header("client") client: String,
        @Header("uid") uid: String
    ): Response<EnterpriseResponse>

    @GET(GET_ENTERPRISE_LIST_PATH)
    suspend fun getFilteredEnterprises(
        @Header("access-token") access_token: String,
        @Header("client") client: String,
        @Header("uid") uid: String,
        @Query("name") name: String
    ): Response<EnterpriseResponse>
}

const val SIGN_IN_PATH = "users/auth/sign_in"
const val GET_ENTERPRISE_LIST_PATH = "enterprises"