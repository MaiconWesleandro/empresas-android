package br.com.maicon.ioasys.ui.home

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import br.com.maicon.ioasys.R
import br.com.maicon.ioasys.app.BaseFragment
import br.com.maicon.ioasys.data.model.EnterpriseModel
import br.com.maicon.ioasys.presentation.HomeViewModel
import br.com.maicon.ioasys.ui.home.adapter.EnterpriseAdapter
import br.com.maicon.ioasys.ui.home.adapter.OnItemClickListener
import kotlinx.android.synthetic.main.fragment_home.*
import kotlinx.android.synthetic.main.search_bar.*
import org.koin.androidx.viewmodel.ext.android.viewModel


class HomeFragment : BaseFragment(), OnItemClickListener<EnterpriseModel>,
    SearchView.OnQueryTextListener {
    private lateinit var adapter: EnterpriseAdapter
    private val viewModel: HomeViewModel by viewModel()
    private val args: HomeFragmentArgs by navArgs()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeEvents()
        setupView()
    }

    override fun onStop() {
        super.onStop()
        searchView.isIconified = true
        searchView.isIconified = true
    }

    private fun setupView() {

        searchView.setOnSearchClickListener {
            imageViewIoasysHome.visibility = View.GONE
            searchView.maxWidth = Integer.MAX_VALUE
            textViewInformation.visibility = View.GONE
            recyclerViewList.visibility = View.VISIBLE
            viewModel.fetchEnterpriseList(args.customHeader)
        }

        searchView.setOnQueryTextListener(this)

        searchView.setOnCloseListener {
            imageViewIoasysHome.visibility = View.VISIBLE
            textViewInformation.visibility = View.VISIBLE
            recyclerViewList.visibility = View.GONE
            progressBarHomeLoading.visibility = View.GONE
            textViewInformationError.visibility = View.GONE
            false
        }
    }

    private fun observeEvents() {
        viewModel.getEnterpriseState().onPostValue(this,
            onLoading = {
                progressBarHomeLoading.visibility = View.VISIBLE
                textViewInformationError.visibility = View.GONE
            },
            onSuccess = {
                progressBarHomeLoading.visibility = View.GONE
                textViewInformationError.visibility = View.GONE

                adapter = EnterpriseAdapter(it, this)
                recyclerViewList.adapter = adapter
                if(it.isEmpty()){
                    textViewInformationError.visibility = View.VISIBLE
                }
            },
            onError = {
                progressBarHomeLoading.visibility = View.GONE
                textViewInformationError.visibility = View.VISIBLE
                textViewInformationError.text = it.message
            }
        )
    }

    override fun onItemClick(item: EnterpriseModel, position: Int) {
        findNavController().navigate(HomeFragmentDirections.actionHomeFragmentToDetailsFragment(item))
    }

    override fun onQueryTextSubmit(query: String?): Boolean {
        return if (query != null){
            viewModel.fetchFilteredEnterpriseList(args.customHeader, query)
            true
        }else{
            false
        }
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        return true
    }


}