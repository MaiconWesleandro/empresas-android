package br.com.maicon.ioasys.data.model

import com.google.gson.annotations.SerializedName

data class EnterpriseResponse(
    val enterprises: List<EnterpriseModel>
)