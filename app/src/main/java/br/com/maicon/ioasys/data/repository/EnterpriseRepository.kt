package br.com.maicon.ioasys.data.repository

import android.accounts.NetworkErrorException
import br.com.maicon.ioasys.data.model.CustomHeadersModel
import br.com.maicon.ioasys.data.model.EnterpriseResponse
import br.com.maicon.ioasys.data.service.ServiceFactory
import br.com.maicon.ioasys.data.utils.ConnectivityHelper
import br.com.maicon.ioasys.data.utils.Result

class EnterpriseRepository(
    private val serviceFactory: ServiceFactory,
    private val connectivityHelper: ConnectivityHelper
) {

    suspend fun fetchEnterpriseList(customHeadersModel: CustomHeadersModel): Result<EnterpriseResponse?> {
        if (connectivityHelper.checkConnectivity()) {
            return Result.NetworkError(NetworkErrorException())
        } else {
            return try {
                val response = serviceFactory.createClient().getEnterprises(
                    customHeadersModel.accessToken,
                    customHeadersModel.client,
                    customHeadersModel.uid
                )
                if (response.code() == 200) {
                    Result.Success(response.body())
                } else {
                    Result.Error(Exception("Error"))
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }

    suspend fun fetchFilteredEnterpriseList(
        customHeadersModel: CustomHeadersModel,
        enterpriseName: String
    ): Result<EnterpriseResponse?> {
        if (connectivityHelper.checkConnectivity()) {
            return Result.NetworkError(NetworkErrorException())
        } else {
            return try {
                val response = serviceFactory.createClient().getFilteredEnterprises(
                    customHeadersModel.accessToken,
                    customHeadersModel.client,
                    customHeadersModel.uid,
                    enterpriseName
                )
                if (response.code() == 200) {
                    Result.Success(response.body())
                } else {
                    Result.Error(Exception("Error"))
                }
            } catch (e: Exception) {
                Result.Error(e)
            }
        }
    }
}