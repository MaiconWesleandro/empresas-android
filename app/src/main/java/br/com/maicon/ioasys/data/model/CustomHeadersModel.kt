package br.com.maicon.ioasys.data.model

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CustomHeadersModel(
    @SerializedName("access-token")
    var accessToken: String = "",
    @SerializedName("client")
    var client: String = "",
    @SerializedName("uid")
    var uid: String = ""
): Parcelable
