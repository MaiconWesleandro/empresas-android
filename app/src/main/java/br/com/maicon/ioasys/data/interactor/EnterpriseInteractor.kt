package br.com.maicon.ioasys.data.interactor

import br.com.maicon.ioasys.data.model.CustomHeadersModel
import br.com.maicon.ioasys.data.model.EnterpriseResponse
import br.com.maicon.ioasys.data.repository.EnterpriseRepository
import br.com.maicon.ioasys.data.utils.Result
import br.com.maicon.ioasys.utils.ThreadContextProvider
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class EnterpriseInteractor(
    private val contextProvider: ThreadContextProvider,
    private val enterpriseRepository: EnterpriseRepository,
    private val scope: CoroutineScope
) {

    fun fetchEnterpriseList(
        customHeadersModel: CustomHeadersModel,
        result: (Result<EnterpriseResponse?>) -> Unit
    ) {
        scope.launch(contextProvider.io) {
            val response = enterpriseRepository.fetchEnterpriseList(customHeadersModel)
            withContext(contextProvider.main) {
                result(response)
            }
        }
    }

    fun fetchFilteredEnterpriseList(
        customHeadersModel: CustomHeadersModel,
        enterpriseName: String,
        result: (Result<EnterpriseResponse?>) -> Unit
    ) {
        scope.launch(contextProvider.io) {
            val response = enterpriseRepository.fetchFilteredEnterpriseList(customHeadersModel, enterpriseName)
            withContext(contextProvider.main) {
                result(response)
            }
        }
    }

}


