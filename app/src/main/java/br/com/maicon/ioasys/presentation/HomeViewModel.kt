package br.com.maicon.ioasys.presentation

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleObserver
import br.com.maicon.ioasys.R
import br.com.maicon.ioasys.data.interactor.EnterpriseInteractor
import br.com.maicon.ioasys.data.mapper.EnterpriseMapper
import br.com.maicon.ioasys.data.model.CustomHeadersModel
import br.com.maicon.ioasys.data.model.EnterpriseModel
import br.com.maicon.ioasys.data.model.EnterpriseResponse
import br.com.maicon.ioasys.utils.extensions.*
import br.com.maicon.ioasys.data.utils.Result
import org.koin.core.KoinComponent

class HomeViewModel(application: Application) : AndroidViewModel(application), LifecycleObserver,
    KoinComponent {

    private val enterpriseInteractor: EnterpriseInteractor by getInteractor()
    private val enterpriseState by viewState<List<EnterpriseModel>>()
    fun getEnterpriseState() = enterpriseState.asLiveData()

    fun fetchEnterpriseList(customHeadersModel: CustomHeadersModel) {

        enterpriseState.postLoading()
        enterpriseInteractor.fetchEnterpriseList(customHeadersModel) {
            when (it) {
                is Result.Success -> {
                    enterpriseState.postSuccess(EnterpriseMapper.parse(it.data))
                }
                is Result.Error -> {
                    enterpriseState.postError(getString(R.string.mensageSearchEmpty))
                }
                is Result.NetworkError -> {
                    enterpriseState.postError(getString(R.string.networkError))
                }
            }
        }
    }

    fun fetchFilteredEnterpriseList(
        customHeadersModel: CustomHeadersModel,
        enterpriseName: String
    ) {

        enterpriseState.postLoading()
        enterpriseInteractor.fetchFilteredEnterpriseList(customHeadersModel, enterpriseName) {
            when (it) {
                is Result.Success -> {
                    enterpriseState.postSuccess(EnterpriseMapper.parse(it.data))
                }
                is Result.Error -> {
                    enterpriseState.postError(getString(R.string.mensageSearchEmpty))
                }
                is Result.NetworkError -> {
                    enterpriseState.postError(getString(R.string.networkError))
                }
            }
        }
    }

}