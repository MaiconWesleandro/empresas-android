Estes documento README tem como objetivo fornecer informações utilizadas no desenvolvimento do projeto Empresas.

### Justificativa de cada biblioteca adicionada ao projeto como dependência ###

Navigation Versão 2.2.1 - utilizado para percorrer entre fragmentos no projeto, navegação da tela de login para a tela home e depois para a tela de detalhes.
Koin versão 2.0.0 - utilizado para auxiliar no uso da injeção de dependências.
Retrofit Library versão 2.6.0 - foi utilizado para realizar aquisições HTTP Requests de uma API.
Gson versão 2.8.5 - utilizado Para manipulação de elementos Json vindo da API.
Coroutines versão para 1.3.0 - utilizado para trabalhar com recursos em paralelo e realizar aquisições no sistema de forma assíncrona.

### O que você faria se tivesse mais tempo ###



### Instruções de como executar a aplicação ###

Ao abrir o aplicativo será exibido uma tela de login, o usuário deve se autenticar.
Para realizar teste deve-se inserir os respectivos dados: E-mail: testeapple@ioasys.com.br Senha: 12341234.
Após autenticado será direcionado para a tela home, onde ao iniciar uma busca o app irá exibir uma lista de empresas.
Ao clicar na empresa selecionada será direcionado para uma tela onde contém detalhes dessa empresa.